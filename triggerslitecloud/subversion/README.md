#### Trigger Deployment
Copy hook scripts to subversion repository hooks/jirahooks directory and create soft links
```
$ ln -s jirahooks/post-commit.sh post-commit
```

#### Customize triggers configurations

1) To run hooks against different configuration file (default is svn_cfg.json)
edit pre/post-commit.sh > specify --config option

`$svnhook --config "modem_cfg.json" ...`

2) To process branches and tag names (default is false) e.g.

`"svn.branch.tags.processing" : true`
