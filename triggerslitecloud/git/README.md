#### Trigger Deployment
Copy hook scripts to git repository (hooks/jirahooks) directory and create soft links
```
$ cd /export/repos/git_repo/hooks

### Post receive hook (Server-side)
$ ln -s jira_hooks/post-receive.sh post-receive

```

#### Customize triggers configurations

1) To run triggers against different configuration file (default is git_cfg.json)
edit post-receive.sh > define the option --config

`-config "modem_cfg.json"`
