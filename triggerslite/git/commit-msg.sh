#!/usr/bin/env bash

# read hook inputs
message_file=$1

# define required vars
python_path=python
git_hook=`dirname $0`/jira_hooks/git_commit_msg.py

# execute
${python_path} ${git_hook} --config "git_cfg.json" --message-file ${message_file} || exit 1

exit 0