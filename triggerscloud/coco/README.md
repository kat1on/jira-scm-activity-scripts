#### Trigger Deployment
Copy Collaborator phase change hook and configure below inputs for phase change trigger at COCO admin console.
```
Executable: python
Parameters: /home/root/coco_phase_change.py -id ${review.id} -name it -title "${review.title}"
        -phase ${review.phase} -creator ${review.creator.login} -date ${review.datecreated}
        -summary "${review.shortchangesummary}" -defects ${review.defectlog}
```

#### Customize triggers configurations

1) To run triggers against different configuration file (default is coco_cfg.json)
edit coco_phase_change.py > navigate to main() function > define config file name inside below constructor

`g = CocoPhaseChangeHook("modem_cfg.json")`
