### Python Version ####
We tested hooks with Python version 2.7

#### Trigger Deployment
Copy hook scripts to subversion repository hooks/jirahooks directory and create soft links
```
$ cd <subversion repo>/hooks
$ ln -s jirahooks/post-commit.sh post-commit
```

#### Customize triggers configurations

1) To run hooks against different configuration file (default is svn_cfg.json)
edit pre/post-commit.sh > specify --config option

`$svnhook --config "modem_cfg.json" ...`

2) To process modified files (default is false) e.g.

`"svn.files.processing" : true`
