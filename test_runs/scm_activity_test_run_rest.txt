curl -s -u "admin:admin" -XPOST -H "Content-Type: application/json" --data "{\"issueKey\":\"DT-1\",\"changeId\":\"100\",\"changeType\":\"perforce\",\"changeDate\":\"2016-05-01 00:00:00\",\"changeStatus\":\"submitted\",\"changeAuthor\":\"john\",\"changeLink\":\"http://perforceweb/c=100\",\"changeMessage\":\"Fixed bug for software X DT-1\",\"changeFiles\":[{\"fileName\":\"/depot/src/AppEntityManager.java\",\"fileAction\":\"Modified\",\"fileVersion\":\"23\"}]}" http://localhost:8080/rest/scmactivity/1.0/changeset/activity

curl -s -u "admin:admin" -XPOST -H "Content-Type: application/json" --data "{\"issueKey\":\"DT-1\",\"changeId\":\"eae95f8\",\"changeType\":\"git\",\"changeDate\":\"2016-05-02 00:00:00\",\"changeAuthor\":\"vprasad79@hotmail.com\",\"changeLink\":\"http://gitweb/c=eae95f8\",\"changeMessage\":\"Fixed bug X for software X DT-1\",\"changeFiles\":[{\"fileName\":\"/src/AppEntityManager.java\",\"fileAction\":\"Modified\",\"fileVersion\":\"23\"}]}" http://localhost:8080/rest/scmactivity/1.0/changeset/activity

curl -s -u "admin:admin" -XPOST -H "Content-Type: application/json" --data "{\"issueKey\":\"DT-1\",\"changeId\":\"eae95f8\",\"changeType\":\"git\",\"changeDate\":\"2016-05-02 00:00:00\",\"changeAuthor\":\"vprasad79@hotmail.com\",\"changeLink\":\"http://gitweb/c=eae95f8\",\"changeMessage\":\"Fixed bug X for software X DT-1\",\"changeFiles\":[{\"fileName\":\"/src/AppEntityManager.java\",\"fileAction\":\"Added\"}, {\"fileName\":\"/src/AppEntityConn.java\",\"fileAction\":\"Modified\"}],\"changeUpdate\":\"true\"}"] http://localhost:8080/rest/scmactivity/1.0/changeset/activity

curl -s -u "admin:admin" -XGET http://localhost:8080/rest/scmactivity/1.0/changeset/activity/DT-1

curl -s -u "admin:admin" -XGET http://localhost:8080/rest/scmactivity/1.0/changeset/activity/DT-1/last

curl -s -u "admin:admin" -XDELETE http://localhost:8080/rest/scmactivity/1.0/changeset/activity/unset/by/perforce/DT-1/100