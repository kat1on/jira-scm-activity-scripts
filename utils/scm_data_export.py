#!/usr/bin/env python

__author__ = "scmenthusiast@gmail.com"
__version__ = "1.0"

import argparse
import MySQLdb
import json
import sys
import os


class ScmActivityExport(object):
    def __init__(self):
        """
        __init__(). prepares or initiates configuration file
        :param: None
        :return: None
        """

        our_config_file = os.path.join(os.path.dirname(__file__), "config.json")

        if os.path.exists(our_config_file):

            try:
                config = json.load(open(our_config_file))

                self.source_db_host = config.get("source.database.host")
                self.source_db_username = config.get("source.database.username")
                self.source_db_password = config.get("source.database.password")
                self.source_db_name = config.get("source.database.name")

                self.scm_activity_tbl = "scm_activity"
                self.scm_message_tbl = "scm_message"
                self.scm_files_tbl = "scm_files"
                self.scm_job_tbl = "scm_job"

            except ValueError, e:
                print (e)
                sys.exit(1)
        else:
            print("Error: config.json file not exists in current path!")
            sys.exit(1)

    def run(self, args):

        connection = MySQLdb.connect(self.source_db_host, self.source_db_username, self.source_db_password, self.source_db_name)
        cursor = connection.cursor(MySQLdb.cursors.DictCursor)

        if args.from_date and args.to_date:
            sql = "SELECT t1.ID, t1.issueKey, t1.changeId, t1.changeType, t1.changeDate, t1.changeAuthor, t1.changeLink, " \
              "t1.changeBranch, t1.changeTag, t1.changeStatus, t2.message FROM {0} t1 LEFT JOIN {1} t2 " \
              "ON t1.ID=t2.scmActivityID WHERE t1.changeDate between '{2}' AND '{3}'".format(
                    self.scm_activity_tbl, self.scm_message_tbl, args.from_date,
                    args.to_date);
        elif args.from_date:
            sql = "SELECT t1.ID, t1.issueKey, t1.changeId, t1.changeType, t1.changeDate, t1.changeAuthor, t1.changeLink, " \
              "t1.changeBranch, t1.changeTag, t1.changeStatus, t2.message FROM {0} t1 LEFT JOIN {1} t2 " \
              "ON t1.ID=t2.scmActivityID WHERE t1.changeDate > '{2}'".format(self.scm_activity_tbl,
                    self.scm_message_tbl, args.from_date);
        elif args.to_date:
            sql = "SELECT t1.ID, t1.issueKey, t1.changeId, t1.changeType, t1.changeDate, t1.changeAuthor, t1.changeLink, " \
              "t1.changeBranch, t1.changeTag, t1.changeStatus, t2.message FROM {0} t1 LEFT JOIN {1} t2 " \
              "ON t1.ID=t2.scmActivityID WHERE t1.changeDate < '{2}'".format(self.scm_activity_tbl,
                    self.scm_message_tbl, args.to_date);
        else:
            sql = "SELECT t1.ID, t1.issueKey, t1.changeId, t1.changeType, t1.changeDate, t1.changeAuthor, t1.changeLink, " \
              "t1.changeBranch, t1.changeTag, t1.changeStatus, t2.message FROM {0} t1 LEFT JOIN {1} t2 " \
              "ON t1.ID=t2.scmActivityID".format(self.scm_activity_tbl, self.scm_message_tbl);

        cursor.execute(sql)

        rows = cursor.fetchall()

        change_set_list = []

        for row in rows:
            scm_id = row.get("ID")

            change_set = {
                "issueKey" : row.get("issueKey"),
                "changeId" : row.get("changeId"),
                "changeType" : row.get("changeType"),
                "changeDate" : row.get("changeDate"),
                "changeAuthor" : row.get("changeAuthor"),
                "changeLink" : row.get("changeLink"),
                "changeBranch" : row.get("changeBranch"),
                "changeTag" : row.get("changeTag"),
                "changeStatus" : row.get("changeStatus"),
                "changeMessage" : row.get("message")
            }

            # change files
            change_files = []

            cursor.execute("SELECT * FROM {0} WHERE scmActivityID = %s".format(self.scm_files_tbl), [scm_id])

            file_rows = cursor.fetchall()

            for file_row in file_rows:
                change_file = {
                    "fileName" : file_row.get("fileName"),
                    "fileAction" : file_row.get("fileAction"),
                    "fileVersion" : file_row.get("fileVersion")
                }
                change_files.append(change_file)

            change_set.update(changeFiles=change_files)

            # scm jobs
            scm_jobs = []

            cursor.execute("SELECT * FROM {0} WHERE scmActivityID = %s".format(self.scm_job_tbl), [scm_id])

            job_rows = cursor.fetchall()

            for job_row in job_rows:
                job_item = {
                    "jobName" : job_row.get("jobName"),
                    "jobStatus" : job_row.get("jobStatus"),
                    "jobLink" : job_row.get("jobLink")
                }
                scm_jobs.append(job_item)

            change_set.update(scmJobs=scm_jobs)

            change_set_list.append(change_set)

        print "[Info] Total Change Sets > {0}".format(len(rows))

        connection.close()

        if args.export_file:
            with open(args.export_file, "w") as outfile:
                json.dump(change_set_list, outfile, indent=4, default=unicode)


def main():
    parser = argparse.ArgumentParser(description='SCM Activity Database Export Script')
    parser.add_argument("--export-file", help='Required Export File Path`', required=True)
    parser.add_argument("--from-date", help='Optional From Date time')
    parser.add_argument("--to-date", help='Optional To Date time')
    args = parser.parse_args()

    j = ScmActivityExport()
    j.run(args)


if __name__ == '__main__':
    main()
