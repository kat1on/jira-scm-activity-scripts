#!/usr/bin/env python

__author__  = "scmenthusiast@gmail.com"
__version__ = "1.0"

from jira_commit_lib import JiraCommitHook
from jira_commit_lib import ScmActivity
from json import load, loads, dumps
from datetime import datetime
import argparse
import sys
import os


class CocoPhaseChangeHook(object):

    def __init__(self, config="coco_cfg.json"):
        """
        __init__(). prepares or initiates configuration file
        :return: None
        """

        self.config_file = os.path.join(os.path.dirname(__file__), config)

        if self.config_file and os.path.exists(self.config_file):
            try:
                config = load(open(self.config_file))

                self.coco_web = config.get("collaborator.web")

                self.jch = JiraCommitHook(config)

            except ValueError, e:
                print (e)
                exit(1)
        else:
            print ("Config file ({0}) not exists!".format(config))
            exit(1)

    def run(self, args):
        """
        run(). executes the jira update for the given review-set
        :param args: Sys args
        :return: None
        """

        changeset = ScmActivity()
        changeset.changeId = args.id
        # Preferred format: ChangeType_Instance Name e.g. coco_ccx
        changeset.changeType = "{0}_{1}".format('collaborator', args.name)
        changeset.changeAuthor = args.creator
        changeset.changeStatus = args.phase
        changeset.changeDate = datetime.strptime(args.date,'%a %b %d %H:%M:%S %Y').strftime('%Y-%m-%d %H:%M:%S')
        changeset.changeLink = self.coco_web.format(args.id)

        # set custom message
        review_message = []
        review_message.append("{0}".format(args.title.strip()))
        review_message.append("Delect Log: {0}".format(args.defects))
        review_message.append("Change Summary: {0}".format(args.summary))
        review_message.append("(i) Phase: {0}".format(args.phase))
        changeset.changeMessage = '\n'.join(review_message)

        matched_issue_keys = self.jch.pattern_validate(args.title)

        'print dumps(changeset.__dict__, indent=4)'
        'print matched_issue_keys'

        if len(matched_issue_keys.keys()) > 0:
            self.jch.jira_update(changeset, matched_issue_keys.keys(),1)


def main():
    """
    main(). parses sys arguments for execution
    :param: None
    :return: None
    """

    parser = argparse.ArgumentParser(description='SCM Activity COCO Review Phase Change Execution Script')
    parser.add_argument("-id", help='Required Review ID', required=True)
    parser.add_argument("-name", help='Required Collaborator instance name', required=True)
    parser.add_argument("-title", help='Required Review Title', required=True)
    parser.add_argument("-phase", help='Required Review Phase', required=True)
    parser.add_argument("-creator", help='Required Review Creator', required=True)
    parser.add_argument("-date", help='Required Review Created Date', required=True)
    parser.add_argument("-summary", help='Required Change Summary', required=True)
    parser.add_argument("-defects", help='Required Defect Log', required=True)

    args = parser.parse_args()

    # g = CocoPhaseChangeHook("modem_cfg.json") # for custom configuration file
    g = CocoPhaseChangeHook() # this uses default config
    g.run(args)


if __name__ == '__main__':
    main()
